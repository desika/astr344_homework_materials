#homework 3

import numpy as np
import matplotlib.pyplot as plt

#Load both data sets.

model_data = np.loadtxt("model_smg.dat")
observable_data = np.loadtxt("ncounts_850.dat")

"""
Compute dN/ds and plot this vs. s on same axis as the observable data
"""
x = model_data[:,0]
y = model_data[:,1]
dif = np.array([])

# First use the forward differential on the first point
h2 = abs(x[1]-x[0]) # first forward grid spacing
dif = np.append(dif,(y[1]-y[0])/h2)

# Use the average differential to compute both
for i in range(1,len(x)-1):
    # iteratively check over the data and compute delta x and use this to compute numerical differential
    h1 = abs(x[i]-x[i-1]) # backward grid spacing
    h2 = abs(x[i]-x[i+1]) # forward grid spacing
    tmp_dif = ((h1)/(h2*(h1+h2))*(y[i+1]))-(((h1-h2)/(h2*h1))*(y[i]))-((h2/(h1*(h1+h2)))*(y[i-1]))
    dif = np.append(dif,tmp_dif) # I am just using this intermediate step to be clear

# Use the backwards differential on the last point
h1 = abs(x[len(x)-1]-x[len(x)-2])
y = np.append(dif,(y[len(x)-1]-y[len(x)-2])/h1)

"""
The Model Data Numerically differentiated
Here I needed to take the absolute value because I accidentally chose h1 & h2 in the 'wrong' direction
"""
x = abs(x)
y = abs(y)
plt.subplot(111)
plt.loglog(x,y,"b-",lw=15.0)


"""
Observable data
"""   
N = observable_data[:,1]
s = observable_data[:,0]
# Unlog the observed data points. Take every N_i s_i and 10^(s_i) etc...
tmp = np.array([])
for i in N:
    tmp = np.append(tmp,10**i)
N = tmp

tmp= np.array([])
for i in s:
    tmp = np.append(tmp,10**i)
s = tmp
plt.loglog(s,N,"ro") # a loglog scatter plot

# Plot extras
plt.legend(("Model Data","Observed Data"))
plt.xlabel("$s$")
plt.ylabel("$dN/ds$")
plt.title("Differential Number Counts: HW 3")
plt.savefig("hw3.png")
