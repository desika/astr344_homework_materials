import matplotlib.pyplot as plot
import numpy as np

# This function will take a function f(x) and integrate it from start to finish
# using n points. This expects that f(x) itself will also be a python function
# that evaluates the function w/ for the number given in the arg

def trapz_integ(f_of_x, start, end, n):
	width = (end - start)/n
	total_sum = 0
	my_list = np.arange
	for i in np.linspace(start, end, num = n):
		if i == start or i == end:
			total_sum += 0.5 * f_of_x(i)
		else:
			total_sum += f_of_x(i)
	return total_sum * width

def func_r_of_z(z): # Our function that gets integrated
	return 3000. / np.sqrt((0.3 * ((1 + z)**3)) + 0.7)

def func_D_sub_a(z): # Our function for Da
	num_spacings = 100
	return trapz_integ(func_r_of_z, 0, z, num_spacings) / (1 + z)

def func_D_sub_a_w_np(z):
	a = np.linspace(0, z, num =100)
	b = map(func_r_of_z, a)
	return np.trapz(b, x = a) / (1 + z)

if __name__ == '__main__':
	my_z_vals = np.linspace(0, 5, num = 100) # Setting my Z2 vals
	my_D_vals = map(func_D_sub_a, my_z_vals) # Getting corresponding Da vals
	my_D_vals_w_np = map(func_D_sub_a_w_np, my_z_vals) # Using np.trapz


	# Plotting Data
	plot.plot(my_z_vals, my_D_vals, 'ro', my_z_vals, my_D_vals_w_np, 'bs')
	plot.ylabel('Da')
	plot.xlabel('z')
	plot.title('Z Value vs. Angular Diameter Distance')
	plot.show()
